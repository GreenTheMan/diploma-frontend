import React, { FormEvent, useState } from "react";
import { topicResponseData } from "src/models/api";
import { searchBarProps } from "src/models/props";

import { config } from "../../../utils/links";
import styles from "./searchBar.module.css";

const searchBarStatus = {
	empty: "",
	loading: "loading",
};

function SearchBar(props: searchBarProps) {
	/**
   *  Hooks
   */

	// Search bar textfield controlled input
	const [query, setQuery] = useState("");

	// previous query, used for memoization.
	const [prevQuery, setPrevQuery] = useState("");

	// loading status of the searchbar
	const [status, setStatus] = useState(searchBarStatus.empty);

	/**
   * Handler functions
   */

	// Search bar form submit handler
	const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
		e.preventDefault();
		setQuery(query.trim());

		if (status === searchBarStatus.loading) {
			return;
		}
		if (prevQuery === query) {
			return;
		}
		setPrevQuery(query);

		// log api call url
		console.log("Data: " + query + ", queried to: " + config.url.API_URL_TOPIC);
		setStatus(searchBarStatus.loading);

		// fetch data through query
		try {
			const response: Response = await window.fetch(config.url.API_URL_TOPIC, {
				method: "POST",
				headers: {
					"content-type": "application/json",
				},
				body: JSON.stringify({
					query: query,
				}),
			});
			const data: topicResponseData = await response.json();

			// log fetched data
			console.log(data);

			props.onResponse(data.result);
		} catch (err) {
			// log err
			console.error(err);
		}

		setStatus(searchBarStatus.empty);
	};

	// Text controlled input field onchange
	const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		setQuery(e.target.value);
	};

	return (
		<form
			className={
				styles.form +
        (status === searchBarStatus.loading ? " " + styles.formLoading : "")
			}
			onSubmit={(e) => handleSubmit(e)}
		>
			<input
				className={styles.searchBar}
				value={query}
				onChange={handleInputChange}
				disabled={status === searchBarStatus.loading ? true : false}
			></input>
			<img
				src="/loading.png"
				alt="loading"
				className={
					status === searchBarStatus.empty
						? styles.iconInitial
						: styles.loadingIcon
				}
			></img>
		</form>
	);
}

export default SearchBar;
