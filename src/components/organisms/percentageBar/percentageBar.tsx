import { percentageBarItem, percentageBarProps } from "src/models/props";

import styles from "./percentageBar.module.css";

function PercentageBar(props: percentageBarProps) {
	return (
		<div className="pb-10 pt-4 md:pb-20 md:pt-8">
			<div className={styles.barsWrapper}>
				{props.readings.map((reading: percentageBarItem, i: number) => {
					return (
						<div
							key={i}
							className={styles.bar}
							style={{
								backgroundColor: reading.color,
								flexBasis: reading.percent + "%",
							}}
						>
							<p
								className={styles.barText}
								style={{
									color: reading.color,
								}}
							>
								{reading.percent ? reading.percent.toFixed(1) : 0}%
							</p>
						</div>
					);
				})}
			</div>
			<div className={styles.legendWrapper + " flex justify-between items-center mx-auto mt-6"}>
				<div className={styles.legendInner}>
					<div className={styles.ballNeg + " rounded-full h-4 w-4"}></div>
					<p className="text-bold">Negative</p>
				</div>
				<div className={styles.legendInner}>
					<div className={styles.ballNeu + " rounded-full h-4 w-4"}></div>
					<p className="text-bold">Neutral</p>
				</div>
				<div className={styles.legendInner}>
					<div className={styles.ballPos + " rounded-full h-4 w-4"}></div>
					<p className="text-bold">Positive</p>
				</div>
			</div>
		</div>
	);
}

export default PercentageBar;
