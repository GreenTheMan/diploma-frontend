import { ArrowNarrowDownIcon } from "@heroicons/react/solid";
import styles from "./dataSection.module.css";
import { dataSectionProps, percentageBarItem } from "src/models/props";
import PercentageBar from "../percentageBar/percentageBar";

// Sentiment ratings and their colors
const colors = {
	positive: "#39BA46",
	neutral: "#9EA09F",
	negative: "#BB0000",
	default: "rgb(243 244 246)",
};

// If sentiment has a score between the max and min value, its overall rating is Neutral.
const neutralBounds = {
	max: 0.25,
	min: -0.25
}

function DataSection(props: dataSectionProps) {
	// Convert current props to PercentageBar's props
	const percentageBarReadings: percentageBarItem[] = [
		{
			name: "Negative",
			value: props.data.Negative,
			percent: (props.data.Negative / props.data.Count) * 100,
			color: colors.negative,
		},
		{
			name: "Neutral",
			value: props.data.Neutral,
			percent: (props.data.Neutral / props.data.Count) * 100,
			color: colors.neutral,
		},
		{
			name: "Positive",
			value: props.data.Positive,
			percent: (props.data.Positive / props.data.Count) * 100,
			color: colors.positive,
		},
	];
	console.log(props);

	return (
		<>
			<div className={styles.arrowWrapper}>
				<ArrowNarrowDownIcon className={styles.arrowDown} />
			</div>
			<div className={styles.dataWrapper}>
				<div className={styles.dataRow}>
					<p>Searching for: <span className={styles.searchTerm}>{props.data.Topic || "none"}</span></p>
				</div>
				<div className={styles.dataRow + " " + styles.dataMidRow}>
					<p>
            Out of{" "}
						<span className={styles.highlightedText}>
							{props.data.Count || 0}
						</span>{" "}
            posts, the Sentiment is:
					</p>
					<PercentageBar readings={percentageBarReadings} />
				</div>
				<div className={styles.dataRow + " " + styles.dataBotRow}>
					<p>
            Average score:{" "}
						<span
							className={styles.highlightedText}
							style={{
								color: !props.data.AvgScore
									? colors.default
									: props.data.AvgScore < -neutralBounds.min
										? colors.negative
										: props.data.AvgScore > neutralBounds.max
											? colors.positive
											: colors.neutral,
							}}
						>
							{props.data.AvgScore > 0 ? "+" : ""}
							{props.data.AvgScore.toFixed(2) || 0}
						</span>
					</p>
					<div className="flex gap-3 items-center">
						<p>Score Range</p>
						<img src="/scoreRange.png" alt="score range"></img>
					</div>
				</div>
			</div>
			<div className={styles.conclusionWrapper}>
				<div className={styles.conclusionBox}>
					<p>
            This topic is:{" "}
						{props.data.AvgScore < -neutralBounds.min ? (
							<span style={{ color: colors.negative }} className="font-bold">
                Negative (-1 to {neutralBounds.min})
							</span>
						) : props.data.AvgScore > neutralBounds.max ? (
							<span style={{ color: colors.positive }} className="font-bold">
                Positive ({neutralBounds.max} to +1)
							</span>
						) : (
							<span style={{ color: colors.neutral }} className="font-bold">
                Neutral ({neutralBounds.min} to {neutralBounds.max})
							</span>
						)}
					</p>
				</div>
			</div>
		</>
	);
}

export default DataSection;
