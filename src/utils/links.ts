interface ServerUrl {
  API_URL: string;
  API_URL_TOPIC: string;
  API_URL_PING: string;
}

interface ServerConfig {
  url: ServerUrl;
}

/**
 * Production
 */

// Url
const prod_url = "https://myapp.herokuapp.com";

// Api config object
const prod: ServerConfig = {
	url: {
		API_URL: "https://myapp.herokuapp.com",
		API_URL_TOPIC: prod_url + "/topic",
		API_URL_PING: prod_url + "/ping",
	},
};

/**
 * Development
 */

// Url
const dev_url = "http://localhost:5000";

// Api config object
const dev: ServerConfig = {
	url: {
		API_URL: dev_url,
		API_URL_TOPIC: dev_url + "/topic",
		API_URL_PING: dev_url + "/ping",
	},
};

export const config: ServerConfig =
  process.env.NODE_ENV === "development" ? dev : prod;
