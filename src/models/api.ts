export interface topicResponseData {
  result: topicData;
}

export interface topicData {
  Topic: string;
  Positive: number;
  Neutral: number;
  Negative: number;
  Count: number;
  AvgScore: number;
}
