import { topicData } from "./api";

export interface searchBarProps {
  onResponse(responseData: topicData): void;
}

export interface dataSectionProps {
  data: topicData;
}

export interface percentageBarItem {
  name: string;
  value: number;
  percent: number;
  color: string;
}

export interface percentageBarProps {
  readings: percentageBarItem[];
}
