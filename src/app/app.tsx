import SearchBar from "components/organisms/search/searchBar";
import { useEffect, useRef, useState } from "react";
import { config } from "../utils/links";

import styles from "./app.module.css";
import DataSection from "components/organisms/dataSection/dataSection";
import { topicData } from "src/models/api";

const App = (): JSX.Element => {
	/**
   *  Hooks
   */

	// State to hold data from searches
	const [result, setResult] = useState({} as topicData);
	const dataSectionRef = useRef<HTMLDivElement>(null);

	/**
   * Handler functions
   */

	// Once searchBar finishes api call, it should call this
	// function passed through props
	const responseReceived = (responseData: topicData) => {
		setResult(responseData);
		dataSectionRef.current.scrollIntoView({ behavior: "smooth" });
	};

	/**
   *  useEffects
   */

	// Test call to backend, Ping -> Pong!
	useEffect(() => {
		console.log("Pinging " + config.url.API_URL_PING);

		// async function to get ping data
		const ping_server = async () => {
			try {
				// Fetch data through query
				const response: Response = await window.fetch(config.url.API_URL_PING, {
					method: "GET",
					headers: {
						"content-type": "application/json",
					},
				});
				const data: JSON = await response.json();
				return data;
			} catch (err) {
				console.error(err);
			}
		};

		// log
		ping_server().then((data) => {
			console.log(data);
		});
	}, []);

	return (
		<main className={styles.main + " px-4 min-h-screen sm:px-0 space-y-16 w-full"}>
			<div className="pt-36 md:pt-44 pb-32 md:pb-16">
				<div className="container mx-auto">
					<div className="flex justify-center items-center flex-col gap-4">
						<a
							target="_blank"
							rel="noreferrer"
							href="https://gitlab.com/GreenTheMan/diploma-frontend"
						>
							<h1 className={"text-5xl block md:text-8xl antialiased font-light tracking-wider text-center " + styles.heading }>Topic.io</h1>
						</a>
						<p className="text-center block text-xs tracking-wider">Search for trending topics</p>
					</div>
					<div className={styles.searchWrapper}>
						<SearchBar onResponse={responseReceived} />
					</div>

					<section className={styles.dataSection + " py-20 font-normal md:text-base text-xs transition-all ease-out duration-300"} ref={dataSectionRef}>
						{Object.keys(result).length === 0 ? null : (
							<DataSection data={result}></DataSection>
						)}
					</section>
				</div>
			</div>
			<footer className={styles.footer + " pt-16 pb-16 max-w-screen-lg xl:max-w-screen-xl mx-auto text-center sm:text-right font-bold"}>
				<a
					href="https://github.com/GreenThaMan"
					rel="noreferrer"
					target="_blank"
				>
          Tengis Buya @ {new Date().getFullYear()}
				</a>
			</footer>
		</main>
	);
};

export default App;
